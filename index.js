/*
	PROBLEM 1
	1. Results of functions to show sum and difference of two numbers on the console.
*/

	//ADDITION
	function addTwoNumbers (num1 , num2)
		{
		console.log	("Displayed sum of " + num1 + " and " + num2) ;	 
		console.log (num1 + num2) ;
		}

	addTwoNumbers (5 , 15) ;

	//SUBTRACTION
	function subtractTwoNumbers (num1 , num2)
		{
		console.log	("Displayed difference of " + num1 + " and " + num2) ;	 
		console.log (num1 - num2) ;
		}

	subtractTwoNumbers (20 , 5) ;



/*
	PROBLEM 2
	2. Values of variables that store the returned values of functions that multiplies and divides two numbers.
*/

	//MULTIPLICATION
	function multiplyTwoNumbers	(num1, num2)
		{
		console.log ("The product of " + num1 + " and " + num2 + ":") ;
		return 	num1 * num2 ;
		}
	let product = multiplyTwoNumbers (50, 10)
	console.log (product)

	//DIVISION
	function divideTwoNumbers	(num1, num2)
		{
		console.log ("The quotient of " + num1 + " and " + num2 + ":") ;
		return 	num1 / num2 ;
		}
	let quotient = divideTwoNumbers (50, 10)
	console.log (quotient)




/*
	PROBLEM 3
	3. Values of variable that store the returned value of function able to calculate area of a circle by its radius
*/

	//AREA COMPUTATION
	function getAreaCircle (radius)
		{
		console.log ("The result of getting the area of a circle with " + radius +  " radius. :") ;
		let pie = 3.14159 ;
		return pie * radius **2 ;
		}
	let circleArea = getAreaCircle (15) ;
	console.log (circleArea) ;


/*
	PROBLEM 4
	4. Value of variable which stores the result of a function that calculates the average of 4 numbers.
*/
	
	//AVERAGING 
	function getAverage (num1 , num2, num3, num4)
		{
		console.log ("The average of " + num1 + ", " + num2 + ", " + num3 + ", " + num4 + ":") ;
		return 	(num1 + num2 + num3 + num4) / 4 ;
		}
	let averageVar = getAverage (20, 40, 60, 80) ;
	console.log	(averageVar) ;

/*
	PROBLEM 5
	5. Value of variable which stores the result of a function that checks if a scoring percentage is passed or failed.
*/

	//GET PASSING SCORE
	function getScoreResult (num1, num2)
		{
		console.log ("Is " + num1 + "/" + num2 + " a passing score?") ;
		let percent = (num1/num2) * 100 ;
		let isPassed = percent >= 75 ;
		return isPassed ;
		}
	let isPassingScore = getScoreResult (38, 50) ;
	console.log (isPassingScore) ;